recurse () {
  # use the arguments to expand the glob
  set -- "${1-.}" "${1-.}"/*

  if [ -e "$2" ]; then
    # there's at least 1 file/dir
    shift

    for file do
      if [ -d "$file" ]; then
        # found a directory, repeat
        recurse "$file"
      fi
      printf "%s\n" "$file"
    done

  fi
}